<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Intervention\Image\ImageManagerStatic;

class Evenements extends Model {

    public $fillable = array('id','name','date_debut','date_fin','description','user_id', 'resultat','rue','ville','img');

    public function users()
    {
        return $this->hasMany('App\User');
    }


    public function getImageAttribute($img){
        if($img){
            return "/img/evenements/{$this->id}.jpg";
        }
        return false;
    }


    public function setImageAttribute($img){
        if(is_object($img)){
            ImageManagerStatic::make($img)->fit(200,200)->save(public_path() . "/img/evenements/{$this->id}.jpg");
            $this->attributes['img'] = true;
        }
    }

}
