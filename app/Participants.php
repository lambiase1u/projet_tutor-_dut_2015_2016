<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 20/09/2015
 * Time: 00:31
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Participants extends  Model {


    public $fillable = ['id','user_id','id_event','id_epreuve', 'id_visiteur'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function visiteurs()
    {
        return $this->belongsToMany('App\Visiteur');
    }

    public function evenements()
    {
        return $this->belongsToMany('App\Evenements');
    }

    public function epreuves()
    {
        return $this->belongsToMany('App\Epreuve');
    }

}