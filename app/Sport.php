<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Sport extends Model {

    public $fillable = ['id', 'nom'];

}
