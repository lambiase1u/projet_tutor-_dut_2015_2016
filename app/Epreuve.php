<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Epreuve extends Model {

    public $fillable = array('name','description','duree','id_event','token');

    public function evenements()
    {
        return $this->hasMany('App\Evenements');
    }
}
