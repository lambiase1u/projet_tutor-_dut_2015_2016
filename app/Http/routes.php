<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/** welcomeController */
Route::get('/', 'WelcomeController@index');
Route::get('home', 'HomeController@index');
Route::get('homeAdmin', 'HomeController@index');

/** UsersController */
Route::get('profil', ['uses' => 'UsersController@edit', 'as' => 'profil']);
Route::get('profil/{id}',['uses' => 'UsersController@show','as' => 'show']);
Route::post('profil', ['uses' => 'UsersController@update']);
Route::post('evenement', ['uses' =>'UsersController@edit', 'as' => 'evenement']);

/** EvenementController */
Route::resource('evenement', 'EvenementController');
Route::get('event/create',['uses' =>'EvenementController@createEvent', 'as' => 'createEvent']);
Route::get('event/{id}',['uses' =>'EvenementController@showEvent', 'as' => 'showEvent']);
Route::get('mesEvenements',['uses' =>'EvenementController@show', 'as' => 'mesEvenements']);
Route::get('mesParticipations',['uses' => 'EvenementController@showParticipationAsUser', 'as' => 'mesParticipations']);
Route::get('mesEvenements/{id}',['uses' =>'EvenementController@showEventAsUser', 'as' => 'showEventAsUser']);;
Route::get('event/{id}/inscription', ['uses' => 'EvenementController@inscription', 'as' => 'inscription']);
Route::get('event/{id}/deinscription', ['uses' => 'EvenementController@deinscription', 'as' => 'deinscription']);
Route::post('evenement/search', 'EvenementController@search');
Route::post('photo', 'EvenementController@changeImage');

/** EpreuveController */
Route::resource('epreuve', 'EpreuveController');
Route::get('epreuve',['uses' => 'EpreuveController@create', 'as' => 'epreuve']);
Route::get('event/{id}/participer', ['uses' => 'EpreuveController@participer', 'as' => 'participer']);
Route::get('resultat/{id}', ['uses' =>'EpreuveController@showResultat', 'as' => 'showResultat']);
Route::get('participate/{token}', ['uses' => 'EpreuveController@participerViaToken']);
Route::get('resultat/{id}/edit', ['uses' =>'EpreuveController@editResultat', 'as' => 'editResultat']);
Route::post('resultat/import', ['uses' => 'EpreuveController@importResultat', 'as' => 'importResultat']);
Route::get('resultat/{id}/exportExcel', 'EpreuveController@exportExcel');

Route::get('event/{id}/neplusparticiper', ['uses' => 'EpreuveController@nePlusParticiper', 'as' => 'nePlusparticiper']);
Route::get('epreuve/{id}/liste', ['uses' => 'EpreuveController@showParticipants', 'as' => 'showParticipants']);

/** AuthController */
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/** MembreController */
Route::get('membres', 'MembresController@index');
Route::post('profil/membre', 'MembresController@searchProfil');

/** SportController */
Route::get('sports', 'SportController@index');

/** VisiteurController */
Route::post('visiteur/register', 'VisiteurController@enregistrer');

/** Contacts */
Route::get('contacts', 'ContactController@index');