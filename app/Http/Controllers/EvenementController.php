<?php namespace App\Http\Controllers;

use App\Epreuve;
use App\Evenements;
use App\Http\Controllers\Controller;
use App\Http\Requests\EvenementRequest;
use App\Http\Controllers\Auth\AuthController;
use App\Participants;
use App\Services\Registrar;

use App\User;
use App\Visiteur;
use App\Sport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic;
use Intervention\Image\Response;
use League\Flysystem\File;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Session\Session;


class EvenementController extends Controller
{

    /**
     *
     */
    public function index()
    {
        $evenement = Evenements::all();
        return view('evenement.index', compact('evenement'));
    }

    /**
     *
     */
    public function create()
    {
        $evenement = new Evenements();
        return view('evenement.create', compact('evenement', 'sport'));
    }


    /**
     *
     */
    public function edit($id)
    {
        $evenement = Evenements::findOrFail($id);
        $sport = Sport::findOrNew($evenement->id_sport);
        return view('evenement.edit', compact('evenement','sport'));
    }


    /**
     *
     */
    public function store(Request $request)
    {
        $id = Auth::User()->id;
        $evenement = new Evenements();
        $evenement->name = $request->name;
        $evenement->date_debut = $request->date_debut;
        $evenement->date_fin = $request->date_fin;
        $evenement->description = $request->description;
        $evenement->user_id = $id;
        $evenement->ville = strtoupper($request->ville);
        $evenement->rue = $request->rue;
        $evenement->save();
        $epreuve = null;
        return view('evenement.showEvent',compact('evenement','epreuve'))->with('success', 'L\'évènement à bien été modifiée');
    }

    /**
     *
     */
    public function update($id, Request $request)
    {
        $evenement = Evenements::findOrFail($id);
        $evenement->name = $request->name;
        $evenement->date_debut = $request->date_debut;
        $evenement->date_fin = $request->date_fin;
        $evenement->description = $request->description;
        $evenement->ville = strtoupper($request->ville);
        $evenement->rue = $request->rue;
        $evenement->save();
        $epreuve = Epreuve::where('id_event', '=', $evenement->id)->get();
        return view('evenement.showEvent',compact('evenement','epreuve'))->with('success', 'L\'évènement à bien été modifiée');
    }


    /**
     *
     */
    public function destroy($id)
    {
        $evenement = Evenements::findOrFail($id);
        $epreuves = Epreuve::where('id_event', '=', $id);
        $index = Participants::where('id_event', '=', $id);
        foreach($epreuves as $e){
            $e->destroyResultat($e);
        }
        $epreuves->delete();
        $index->delete();
        if($evenement->img == true){
            if (file_exists(public_path() . '/img/evenements' . $evenement->id . 'jpg'))
                \Illuminate\Support\Facades\File::delete(public_path() . '/img/evenements' . $evenement->id . 'jpg');
        }
        $evenement->delete();
        return redirect(action('EvenementController@index'))->with('success', 'L\'évènement à bien été supprimé');
    }

    /**
     *
     */
    public function show()
    {
        $idUser = Auth::User()->id;
        $evenement = Evenements::where('user_id', '=', $idUser)->get();
        $user = User::findOrFail($idUser);
        return view('evenement.myEvents', compact('evenement', 'user'));
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function  showEventAsUser($id)
    {
        $evenement = Evenements::where('user_id', '=', $id)->get();
        $user = User::findOrFail($id);
        return view('evenement.myEvents', compact('evenement', 'user'));
    }

    public function showParticipationAsUser()
    {
        $id = Auth::User()->id;
        $participations = Participants::where('user_id', '=', $id)->get();
        $user = User::findOrFail($id);
        $evenement = array();
        foreach ($participations as $p) {
            $evenements = Evenements::where('id', '=', $p['id_event'])->get();
            foreach ($evenements as $e) {
                array_push($evenement, $e);
            }
        }
        return view('evenement.myParticipations', compact('evenement', 'user'));

    }


    /**
     *
     */
    public function showEvent($id)
    {
        $evenement = Evenements::findOrFail($id);
        $epreuve = Epreuve::where('id_event', '=', $id)->get();
        return view('evenement.showEvent', compact('evenement', 'epreuve'));
    }


    public function search()
    {
        $terms = Input::get('search');
        $evenement = Evenements::where('name', 'LIKE', '%' . $terms . '%')
            ->orWhere('description', 'LIKE', '%' . $terms . '%')
            ->orWhere('date_debut', '=', $terms)
            ->orWhere('date_fin', '=', $terms)
            ->orWhere('ville', 'LIKE', $terms . '%')
            ->get();
        return view('evenement.index', compact('evenement'));
    }

    public  function  changeImage(Request $request){

        $evenement = Evenements::findOrFail($request->id);
        $manager = new ImageManager();
        if(isset($_FILES['img']['tmp_name'])&& !empty($_FILES['img']['tmp_name'])){
            $manager->make($_FILES['img']['tmp_name'])->fit(200,200)->save(public_path('/img/evenements/'.$request->id.'.jpg'));
            $evenement->img = true;
        }
        $evenement->save();
        $epreuve = Epreuve::where('id_event', '=', $evenement->id)->get();
        return view('evenement.showEvent',compact('evenement','epreuve'));
    }
}
