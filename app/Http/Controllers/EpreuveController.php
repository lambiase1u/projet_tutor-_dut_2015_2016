<?php namespace App\Http\Controllers;

use App\Epreuve;
use App\Evenements;
use App\Http\Requests;


use App\Participants;
use App\Sport;
use App\User;
use App\Visiteur;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class EpreuveController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $epreuve = Epreuve::all();
        return view('epreuve.index', compact('epreuve'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $epreuve = new Epreuve();
        $sport = Sport::all();
        return view('epreuve.create', compact('epreuve', 'sport'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        $token = str_random(10);
        $epreuve = new Epreuve();
        $epreuve->name = $request->name;
        $epreuve->description = $request->description;
        $epreuve->duree = $request->duree;
        $epreuve->id_event = $request->id_event;
        $epreuve->id_sport = $request->sport;
        $epreuve->token = $token;
        $epreuve->save();
        $evenement = Evenements::findOrFail($epreuve->id_event);

        // $epreuve = Epreuve::create($request->only('name','description','date'));
        return redirect(action('EvenementController@showEvent', $evenement))->with('success', 'L\'épreuve à bien été créée');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $epreuve = Epreuve::findOrFail($id);
        $sport = Sport::all();
        return view('epreuve.edit', compact('epreuve', 'sport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id, \Illuminate\Http\Request $request)
    {
        $epreuve = Epreuve::findOrFail($id);
        $epreuve->name = $request->name;
        $epreuve->description = $request->description;
        $epreuve->duree = $request->duree;
        $epreuve->id_event = $request->id_event;
        $epreuve->id_sport = $request->sport;
        $epreuve->save();
        $evenement = Evenements::findOrFail($epreuve->id_event);
        return redirect(action('EvenementController@showEvent', $evenement))->with('success', 'L\'épreuve à bien été modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $epreuve = Epreuve::findOrFail($id);
        $evenement = Evenements::findOrFail($epreuve->id_event);
        $index = Participants::where('id_epreuve', '=', $id);

        $index->delete();
        $epreuve->delete();
        return redirect(action('EvenementController@showEvent', $evenement))->with('success', 'L\'épreuve à bien été supprimé');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($e)
    {
        $epreuve = new Epreuve();
        $epreuve->id_event = $e;
        $sport = Sport::all();
        return view('epreuve.create', compact('epreuve', 'sport'));

    }

    /**
     * @param $e
     */
    public function participer($e)
    {
        $epreuve = Epreuve::findOrFail($e);
        if (Auth::check()) {
            $idUser = Auth::user()->id;
            $evenement = Evenements::findOrFail($epreuve->id_event);
            $participant = new Participants();
            $participant->user_id = $idUser;
            $participant->id_event = $evenement->id;
            $participant->id_epreuve = $epreuve->id;
            $participant->save();
            return redirect()->back()->with('success', 'Vous participez maintenant à l\'épreuve ' . $epreuve->name);
        } else {
            return view('visiteur.participation', compact('epreuve'));
        }
    }

    public function participerViaToken($e)
    {
        $epreuveTab = Epreuve::all();
        $url = '';
        foreach ($epreuveTab as $epreuve) {
            if ($epreuve->token == $e) {
                $url = (url('event/' . $epreuve->id_event));
            }
        }


        return redirect($url);
    }

    public function nePlusParticiper($e)
    {
        $idUser = Auth::user()->id;
        $epreuve = Epreuve::findOrFail($e);
        $evenement = Evenements::findOrFail($epreuve->id_event);
        $participe = Participants::where('user_id', '=', $idUser)->where('id_event', '=', $evenement->id)->where('id_epreuve', '=', $epreuve->id)->get();
        foreach ($participe as $p)
            $p->delete();
        return redirect()->back();
    }

    public static function checkInscris($e)
    {
        $idUser = Auth::user()->id;
        $participer = Participants::where('user_id', '=', $idUser)->where('id_event', '=', $e->id)->where('id_epreuve', '=', NULL)->get();
        if (sizeof($participer) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public static function checkParticiper($e)
    {
        if (Auth::check()) {
            $idUser = Auth::user()->id;
            $participe = Participants::where('user_id', '=', $idUser)->where('id_epreuve', '=', $e->id)->get();
            if (sizeof($participe) == 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function showParticipants($e)
    {
        $epreuve = Epreuve::findOrFail($e);
        $liste = Participants::where('id_epreuve', '=', $epreuve->id)->where('id_event', '=', $epreuve->id_event)->get();
        $user = array();
        $visiteur = array();
        foreach ($liste as $l) {
            if (!empty($l->user_id))
                array_push($user, User::findOrFail($l->user_id));
            elseif (!empty($l->id_visiteur)) {
                array_push($visiteur, Visiteur::findOrFail($l->id_visiteur));
            }
        }

        return view('epreuve.liste', compact('user'), compact('visiteur'));
    }


    /**
     *
     */
    public function editResultat($e)
    {
        $epreuve = Epreuve::findOrFail($e);
        $evenement = Evenements::where('id', '=', $epreuve->id_event)->first();
        return view('epreuve.editResultat', compact('evenement', 'epreuve'));

    }

    /**
     *
     */
    public function destroyResultat($e)
    {
        if (file_exists(public_path() . '/resultats/resultat' . $e->id . '.xls'))
            \Illuminate\Support\Facades\File::delete(public_path() . '/resultats/resultat' . $e->id . '.xls');

    }

    /**
     *
     */
    public function showResultat($e)
    {
        $epreuve = Epreuve::findOrFail($e);
        $evenement = Evenements::findOrFail($epreuve->id_event);
        $id_createur = $evenement->user_id;
        if (file_exists(public_path() . '/resultats/resultat' . $e . '.xls')) {
            $reader = Excel::selectSheetsByIndex(0)->load(public_path() . '/resultats/resultat' . $e . '.xls');
            $liste = array();
            foreach ($reader->toArray() as $row) {
                if (!is_null($row['temps'])) {
                    $row['temps'] = $row['temps']->format('h:i:s');
                    array_push($liste, $row);
                }
            }
            return view('epreuve.showResultat', compact('epreuve', 'liste', 'id_createur'));
        } else {
            return view('epreuve.showResultat', compact('epreuve', 'id_createur'));
        }
    }

    /**
     *
     */
    public function importResultat()
    {
        $epreuve = Epreuve::findOrFail(Input::get('epreuve'));
        $file = array('file' => Input::file('file'));
        $rules = array('file' => 'required', 'mimes:xls, xlsx');
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors('Erreur dans le format du fichier importé : .xls, .xlsx');
        } else {
            if (Input::file('file')->isValid()) {
                $destinationPath = public_path() . '/resultats/';
                $extension = Input::file('file')->getClientOriginalExtension();
                $fileName = 'resultat' . $epreuve->id . '.' . $extension;
                Input::file('file')->move($destinationPath, $fileName);
                $epreuve->resultat = 1;
                $epreuve->save();
                return redirect(action('EpreuveController@showResultat', compact('epreuve')));
            } else {
                return view('evenement.editResultat', compact('epreuve'))->withErrors('Une erreur est survenue lors de l\'import de votre fichier');
            }
        }
    }

    public function exportExcel($e)
    {
        $epreuve = Epreuve::findOrFail($e);
        $participant = Participants::where('id_epreuve', '=', $epreuve->id)->get();
        Excel::create('fiche_resultat', function ($excel) use ($epreuve, $participant) {
            $excel->setTitle('Fiche resultat ' . $epreuve->name);
            $excel->setCreator('Sport_Events')->setCompany('Sport_Events');

            $excel->sheet('Page resultat', function ($sheet) use ($participant, $epreuve) {
                $sheet->cell('A1', function ($cell) {
                    $cell->setValue('NOMS');
                });
                $sheet->cell('B1', function ($cell) {
                    $cell->setValue('DOSSARDS');
                });
                $sheet->cell('C1', function ($cell) {
                    $cell->setValue('TEMPS');
                });
                $sheet->setColumnFormat(array(
                    'C' => 'h:mm:ss;@'
                ));

                if (sizeof($participant) == 0) {
                    return redirect()->back()->with('error', 'pas de participant pour l\'événément');
                } else {
                    $i = 3;
                    $num_dossard = 1;
                    foreach ($participant as $p) {
                        if (!is_null($p->d_visiteur)) {
                            $visiteur = Visiteur::where('id', '=', $p->id_visiteur)->first();
                            $sheet->cell('A' . $i, function ($cell) use ($visiteur) {
                                $cell->setValue($visiteur->name);
                            });


                            $sheet->cell('B' . $i, function ($cell) use ($num_dossard) {
                                $cell->setValue($num_dossard);
                            });


                            $sheet->cell('C' . $i, function ($cell) {
                                $cell->setValue('00:00:00');
                            });


                        } elseif (!is_null($p->user_id)) {
                            $user = User::where('id', '=', $p->user_id)->first();
                            $sheet->cell('A' . $i, function ($cell) use ($user) {
                                $cell->setValue($user->name);
                            });


                            $sheet->cell('B' . $i, function ($cell) use ($num_dossard) {
                                $cell->setValue($num_dossard);
                            });


                            $sheet->cell('C' . $i, function ($cell) {
                                $cell->setValue('00:00:00');
                            });

                        }
                        $i++;
                        $num_dossard++;
                    }
                }

            });
        })->export('xls');
        return view('epreuve.editResultat', compact('epreuve'));

    }


}
