<?php namespace App\Http\Controllers;



use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class MembresController extends Controller
{

    public function index(){
        $membres = User::all();
        return view('membre.index', compact('membres'));
    }

    public function searchProfil(){
        $user = User::where('name', '=', Input::get('goProfil'))->first();
        if($user == Auth::User()) {
            return redirect(url('profil'))->with('success', 'Vous êtes déjà sur votre profil');
        }
        elseif(sizeof($user)==0){
            return redirect()->back()->with('error', 'Aucune correspondance trouvée...');
        }
        else{
            return view('users.show', compact('user'));
        }
    }

}