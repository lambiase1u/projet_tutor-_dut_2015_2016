<?php namespace App\Http\Controllers;

use App\Epreuve;
use App\Evenements;
use App\Http\Requests;
use App\Participants;
use App\User;
use App\Visiteur;
use Illuminate\Support\Facades\Input;

class VisiteurController extends Controller {

    public function enregistrer()
    {
        $epreuve = Epreuve::findorFail(Input::get('epreuve'));
        // permet de réserver le nom de participant pour tous les users et check si le mail correspond à un compte
        $u = User::where('name', '=', Input::get('pseudo'))
            ->orWhere('email', '=', Input::get('email'))
            ->get();
        // check si un user porte le pseudo du visiteur
        if (sizeof($u) != 0){
            return redirect()->back()->with('error', 'Votre adresse mail est déjà enregistrée en tant qu\'utilisateur !');
        }
        $v = Visiteur::where('name', '=', Input::get('pseudo'))
            ->orWhere('email', '=', Input::get('email'))
            ->get();
        if (sizeof($v) == 0) {

            // creation + save du visiteur
            $visiteur = new Visiteur();
            $visiteur->name = Input::get('pseudo');
            $visiteur->email = Input::get('email');
            $visiteur->firstname = Input::get('firstname');
            $visiteur->lastname = Input::get('name');
            $visiteur->age = Input::get('age');
            $visiteur->id_epreuve = $epreuve->id;
            $visiteur->save();
            // mise à jour de la table participant
            $participant = new Participants();
            $participant->id_event = $epreuve->id_event;
            $participant->id_epreuve = $epreuve->id;
            $participant->id_visiteur = $visiteur->id;
            $participant->save();
            $evenement = Evenements::findOrFail($epreuve->id_event);
            return redirect(url('event', compact('evenement')))->with('success', 'Vous participez maintenant à l\'epreuve numero '.$epreuve->id);
        }
        else{
            return redirect()->back()->with('error', 'Pseudo ou adresse mail déjà utilisé(e)');
        }
    }


}
