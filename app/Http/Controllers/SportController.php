<?php namespace App\Http\Controllers;




use App\Http\Controllers;
use App\Sport;

class SportController extends Controller
{

    function index(){

        $sport = Sport::all();
        return view('sport.index', compact('sport'));
    }
}