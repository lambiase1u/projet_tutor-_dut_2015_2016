@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Attention</strong> Certains champs n'ont pas été rempli correctement.
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('success'))
    <div class="card-panel green lighten-2 col m10">
        {{ session('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="card-panel red lighten-2">
        {{ session('error') }}
    </div>
@endif