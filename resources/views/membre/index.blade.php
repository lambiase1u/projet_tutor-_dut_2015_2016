@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <table class="table responsive-table centered">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Email</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Age</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $membres as $m)
                        <tr>
                            <td><div class="chip">
                                    <img src="{{url($m->avatar)}}" alt="Contact Person">
                                    {{$m->name}}
                                </div></td>
                            <td><div class="chip">
                                    {{$m->email}}
                                </div></td>
                            <td>{{ $m->firstname }}</td>
                            <td>{{ $m->lastname }}</td>
                            <td>{{ $m->age }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
