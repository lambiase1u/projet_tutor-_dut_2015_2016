@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel-default">
                    <h3>Nous proposons plusieurs sports</h3>
                    <div class="panel-body">
                        @foreach($sport as $s)
                            <h5 class="text-primary">{{$s->nom}}</h5>
                            <p class="text-primary">{{$s->description}}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection