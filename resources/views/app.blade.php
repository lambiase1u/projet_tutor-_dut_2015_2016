<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}"/>
    <title>Laravel</title>

    <link href="" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('/css/materialize.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="{{ asset('/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<nav>
    <div class="nav-wrapper white">
        <a href="{{ url('/') }}" class="brand-logo ">SportEvents & Co</a>
        <ul class="right hide-on-med-and-down ">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/evenement') }}">Evenements</a></li>
            <li><a href="{{ url('/membres') }}">Membres</a></li>
            <li><a href="{{ url('/contacts') }}">Contacts</a></li>
            <li><a href="{{ url('/sports') }}">Sports</a></li>




            @if (Auth::guest())
                <li><a href="{{ url('/auth/login') }}">Se connecter</a></li>
                <li><a href="{{ url('/auth/register') }}">S'inscrire</a></li>
            @else
                <li><a class="dropdown-button" href="#!" data-activates="dropdown1"> <div class="chip">
                            <img src="{{url(\Illuminate\Support\Facades\Auth::user()->avatar)}}" alt="Contact Person">
                            {{\Illuminate\Support\Facades\Auth::user()->name}}
                        </div><i class="material-icons right">arrow_drop_down</i></a></li>

                <ul id="dropdown1" class="dropdown-content">
                    <li><a href="{{ route('profil') }}">Mon profil</a></li>
                    <li><a href="{{ route('mesEvenements') }}">Mes évènements</a></li>
                    <li><a href="{{ route('mesParticipations') }}">Mes Participations</a></li>
                    <li><a href="{{ url('/auth/logout') }}">Déconnexion</a></li>
                </ul>

                @endif
                        <!-- Dropdown Trigger -->

        </ul>
        <ul id="slide-out" class="side-nav">
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/evenement') }}">Evenements</a></li>
            <li><a href="{{ url('/membres') }}">Membres</a></li>
            <li><a href="{{ url('/contacts') }}">Contacts</a></li>
            <li><a href="{{ url('/sports') }}">Sports</a></li>

            @if (Auth::guest())
                <li><a href="{{ url('/auth/login') }}">Se connecter</a></li>
                <li><a href="{{ url('/auth/register') }}">S'inscrire</a></li>
            @else
                <li><a href="{{ route('profil') }}">Mon profil</a></li>
                <li><a href="{{ route('mesEvenements') }}">Mes évènements</a></li>
                <li><a href="{{ url('/auth/logout') }}">Déconnexion</a></li>


            @endif
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">reorder</i></a>

    </div>


</nav>



<main>
    @include('flash')
    @yield('content')
</main>


<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <p class="col s6">Copyright © 2015 SportEvent & Co, tous droits réservés</p>
                <p class="col s6">dernière mise à jour : 02/11/2015</p>
            </div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{ url('/js/materialize.js') }}"></script>
<script src="{{url('/js/materiaalize.min.js')}}"></script>
<script src="{{ url('/js/init.js') }}"></script>
<script src="{{ url('/js/laravel.js') }}"></script>

</body>
</html>
