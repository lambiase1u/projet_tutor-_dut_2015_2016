
@extends('app')

@section('content')

<div class="container">
    <div class="slider">
        <ul class="slides">
                    <li>


                        <img src="{{url().'/img/slides/slide1.jpg'}}">

                        <div class="caption center-align">
                            <h3></h3>
                            <h5 class="light grey-text text-lighten-3"></h5>
                        </div>
                    </li>
            <li>
                <img src="{{url().'/img/slides/slide2.jpg'}}">
                <div class="caption center-align">
                    <h3></h3>
                    <h5 class="light grey-text text-lighten-3"></h5>
                </div>
            </li>

            <li>
                <img src="{{url().'/img/slides/slide3.jpg'}}">
                <div class="caption center-align">
                    <h3></h3>
                    <h5 class="light grey-text text-lighten-3"></h5>
                </div>
            </li>
        </ul>
    </div>
</div>
    <div class="container">
        <div class="row">
<br>
            <div class="col s12 m5">
                <h4 class="text-primary">Qui sommes-nous </h4>

                <p>Nous sommes un groupe d'étudiants de l'IUT Nancy-Charlemagne en deuxième année de dut
                    informatique.</p>

                <h4 class="text-primary">Fonctionnalités</h4>

                <p class="title">Notre site intègre plusieurs fonctionnalités :
                <ul>
                    <li>un espace utilisateur pour personnaliser son profil</li>
                    <li>un espace MyEvents pour gérer ses événements</li>
                    <li>un onglet Evenements permettant de visualiser tous les événements disponibles au sein de la
                        communauté
                    </li>
                    <li>inscription à des événements pour les utilisateurs avec la possibilité de participer aux
                        épreuves de cet événement
                    </li>
                    <li>une fois vos événements terminés, en tant que propriétaire de l'événement vous pourrez importer
                        les résultats pour que vous et les autres participants puissent les consulter
                    </li>
                </ul>
                </p>
                <h4 class="text-primary">Notre but </h4>

                <p class="title">Nous souhaitons accueillir une communauté dynamique et offrir une communication stable
                    et rapide entre sportifs pour vous organiser au mieux</p>

                <h4 class="text-primary">Contact </h4>

                <p class="title">mail : projetlimaga@gmail.com</p>
            </div>

            <div class="col s12 m6 offset-m1">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <h4>Comment nous rejoindre ?</h4>
                        <h5>Vous êtes organisateur :
                        </h5>
                        <ul>
                            <li>Inscrivez vous sur notre site</li>
                            <li>Allez dans l'onglet <a href="{{ route('evenement') }}">evenements</a></li>
                            <li>Créez un nouvel évènement</li>
                            <li>Invitez des personnes</li>
                            <li>Vous n'avez plus qu'à attendre vos sportif !</li>
                        </ul>
                        </br>

                        <h5>Vous voulez participer :</h5>
                        <ul>
                            <li>Si vous avez reçu un lien d'inscription utilisez le</li>
                            <li>Sinon allez dans l'onglet <a href="{{ route('evenement') }}">evenements</a></li>
                            <li>Cliquez sur l'événement qui vous interesse</li>
                            <li>Choisissez les épreuves auxquelles vous voulez participer</li>
                            <li>Vous n'avez plus qu'à remplir le formulaire</li>
                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection