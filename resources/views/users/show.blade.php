@extends('layouts.sidebar')

@section('main')


    {!! Form::close() !!}

    @if($user->id == \Illuminate\Support\Facades\Auth::id())
        <div class="container">
            <div class="row">
                <div class="col s12 m6">
                    <div class="card">

                        <div class="card-image waves-effect waves-block waves-light">
                            @if($user->avatar)
                                <img class="responsive-img" src="{{ url($user->avatar) }}"/>
                            @endif
                        </div>

                        <div class="card-content">

                            <span class="card-title activator grey-text text-darken-4">{{$user->name}}<i
                                        class=" large material-icons right">playlist_add</i></span>

                        </div>
                        <div class="card-reveal">
                            {!! Form::model($user, ['class' => 'form-horizontal', 'files' => true]) !!}

                            <span class="card-title grey-text text-darken-4">Mes infos<i class="material-icons right">close</i></span>

                            <p><a href="">{!! Form::file('avatar', ['class' => 'form-control']) !!}</a></p>

                            <div class="row">
                                <div class="form-group">
                                    <label class="col s10 offset-s1 m2 offset-m1 control-label">Pseudo</label>

                                    <div class="col s10 offset-s1 m7 ">
                                        {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label class="col s12 m2 offset-m1 control-label">Email</label>

                                    <div class="col s12  m7 ">
                                        {!! Form::email('email', $user->email, ['class' => 'form-control', 'disabled'])
                                        !!}

                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <label class="col s12 m2 offset-m1 control-label">Prénom</label>

                                    <div class="col s12  m7 ">
                                        {!! Form::text('firstname', $user->firstname, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <label class="col s12 m2 offset-m1 control-label">Nom</label>

                                    <div class="col s12  m7 ">
                                        {!! Form::text('lastname', $user->lastname, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label class="col s12 m2 offset-m1 control-label">Age</label>

                                    <div class="col s12  m7 ">
                                        {!! Form::input('number', 'age', $user->age, ['class' => 'form-control']); !!}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <div class="col s10 offset-s2  m6  offset-m4">
                                        <button type="submit" class="btn btn-primary">
                                            modifier
                                        </button>
                                    </div>
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>



                    @else

                        <div class="container">
                            <div class="row">
                                <div class="col s12 m6">
                                    <div class="card">

                                        <div class="card-image waves-effect waves-block waves-light">
                                            @if($user->avatar)
                                                <img class="responsive-img" src="{{ url($user->avatar) }}"/>
                                            @else
                                                <img class="responsive-img"
                                                     src="{{ url(DIRECTORY_SEPARATOR.'imgHold'.DIRECTORY_SEPARATOR.'insertPic.png') }}"/>
                                            @endif
                                        </div>

                                        <div class="card-content">

                            <span class="card-title activator grey-text text-darken-4">{{$user->name}}<i
                                        class=" large material-icons right">playlist_add</i></span>

                                        </div>
                                        <div class="card-reveal">

                                            <span class="card-title grey-text text-darken-4">Infos de {{$user->name}}<i
                                                        class="material-icons right">close</i></span>

                                            <br>

                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col s10 offset-s1 m2 offset-m1 control-label">Pseudo</label>

                                                    <div class="col s10 offset-s1 m7 ">
                                                        {{$user->name}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col s12 m2 offset-m1 control-label">Email</label>

                                                    <div class="col s12  m7 ">
                                                        {{$user->email}}

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col s12 m2 offset-m1 control-label">Prénom</label>

                                                    <div class="col s12  m7 ">
                                                        {{$user->firstname}}
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col s12 m2 offset-m1 control-label">Nom</label>

                                                    <div class="col s12  m7 ">
                                                        {{$user->lastname}}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group">
                                                    <label class="col s12 m2 offset-m1 control-label">Age</label>

                                                    <div class="col s12  m7 ">
                                                        {{$user->age}}
                                                    </div>
                                                </div>
                                            </div>


                                            {!! Form::close() !!}

                                        </div>
                                    </div>
                                    @endif

                                    @endsection

                                    @section('sidebar')

                                        @include('users.sidebar', ['tab' => 'profil'])

                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
@endsection