<div class="col s12 m10 offset-m1">


    <ul class="collection">
        @if(isset($tab))
        <li class="collection-item"><a href="{{ route('profil') }}"
                                       class="list-group-item" {{ $tab =="profil" ? "active" : "" }}>Mon profil</a></li>
        @endif
        <li class="collection-item"><a href="{{ action('EvenementController@showEventAsUser', $user->id) }}"
                                       class="list-group-item">Mes évènements</a></li>
        <li class="collection-item"><a href="{{ action('EvenementController@showParticipationAsUser')}}"
                                       class="list-group-item">Mes participations</a></li>
        @if($user->id != \Illuminate\Support\Facades\Auth::id())
            <li class="collection-item"><a href="{{ action('EvenementController@showEventAsUser', $user->id) }}"
                                           class="list-group-item">évènements de {{$user->name}}</a></li>
        @endif
        </ul>
    <div class="list-group-item">
        Rechercher un profil
        <div class="list-group-item">
            {!! Form::open(array('action' => 'MembresController@searchProfil')) !!}
            {!! Form::text('goProfil', null, ['class' => 'form-control', 'placeholder' => 'Saisir un nom
            d\'utilisateur', 'required']) !!}
            {!! Form::submit('Rechercher') !!}
            {!! Form::close() !!}
        </div>
    </div>

</div>