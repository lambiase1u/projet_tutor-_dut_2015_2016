@extends('layouts.sidebar')

@section('main')

    @if($user->id == \Illuminate\Support\Facades\Auth::id())
            <br><br>
            <div class="row">
                <div class="col s12 m8">
                    <div class="card">
                        <div class="col s12 m6">
                            <div class="card-image ">
                                @if($user->avatar)
                                    <img class="responsive-img" src="{{ url().$user->avatar }}"/>
                                @endif

                            </div>
                        </div>
                        <div class="card-content col s12 m6">

                           <span class="card-title activator grey-text text-darken-4">{{$user->name}}<i
                                       class=" medium material-icons right">mode_edit</i></span>
                            <br><br>
                            <p><b>Nom </b>: {{$user->lastname}}<br>
                                <b> Prénom</b>: {{$user->firstname}}<br>
                                <b> Age</b>: {{$user->age}} <br>
                                <b> Email</b>: {{$user->email}}<br>
                            </p>
                        </div>

                        <div class="card-reveal">
                            {!! Form::model($user, ['class' => 'form-horizontal', 'files' => true]) !!}

                            <span class="card-title grey-text text-darken-4">Mes infos<i class="material-icons right">close</i></span>

                            <p><a href="">{!! Form::file('avatar', ['class' => 'form-control']) !!}</a></p>

                            <div class="row">
                                <div class="form-group">
                                    <label class="col s10 offset-s1 m2 offset-m1 control-label">Pseudo</label>

                                    <div class="col s10 offset-s1 m7 ">
                                        {!! Form::text('name', $user->name, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label class="col s12 m2 offset-m1 control-label">Email</label>

                                    <div class="col s12  m7 ">
                                        {!! Form::email('email', $user->email, ['class' => 'form-control', 'disabled'])
                                        !!}

                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <label class="col s12 m2 offset-m1 control-label">Prénom</label>

                                    <div class="col s12  m7 ">
                                        {!! Form::text('firstname', $user->firstname, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <label class="col s12 m2 offset-m1 control-label">Nom</label>

                                    <div class="col s12  m7 ">
                                        {!! Form::text('lastname', $user->lastname, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <label class="col s12 m2 offset-m1 control-label">Age</label>

                                    <div class="col s12  m7 ">
                                        {!! Form::input('number', 'age', $user->age, ['class' => 'form-control']); !!}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group">
                                    <div class="col s10 offset-s2  m6  offset-m4">
                                        <button type="submit" class="btn btn-primary">
                                            modifier
                                        </button>
                                    </div>
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                    </div>

                    @endif

                    <div class="col s12 m4">
                        @include('users.sidebar', ['tab' => 'profil'])
                        </div>

                </div>

@endsection