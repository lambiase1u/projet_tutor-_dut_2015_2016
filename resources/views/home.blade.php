@extends('app')

@section('content')

    <br>
    <div class="row">
        <div class="col s3 m2">
            <h6>Rechercher un événement</h6>
            {!! Form::open(array('action' => 'EvenementController@search')) !!}
            {!! Form::text('search', null, ['class' => 'input-field','id'=>'search','type' =>'search' , 'placeholder' =>
            'Saisir une info evenement', 'required']) !!}
            {!! Form::submit('rechercher')!!}
            {!! Form::close() !!}
        </div>
        <div class="col s12 m6">
            @if(sizeof($evenements)==0)
                <div class="card">
                    <div class="col s12 m6">
                        <h5>Il n'y a pas d'événement récent</h5>
                    </div>
                </div>
            @else
                @foreach($evenements as $e)
                    <div class="card">
                        <div class="col s12 m6">
                            <div class="card-image ">
                                @if($e->img == false)
                                    <img class="responsive-img" src="{{url().'\imgHold\no-image.png'}}"/>
                                @else
                                    <img class="responsive-img" src="{{url().'\img\evenements\\'.$e->id.'.jpg'}}"/>
                                @endif
                            </div>
                        </div>
                        <div class="col s12 m6">
                            <p>
                                <b>Nom d'évenement :</b> {{ $e->name }} <br>
                                <b>date début :</b> {{$e->date_debut}} <br>
                                <b>date fin :</b> {{$e->date_fin}} <br>
                                <b>Rue : </b> {{$e->rue}}<br>
                                <b>Ville :</b> {{$e->ville }}<br><br>
                                <b>créateur :</b>

                            <div class="chip">
                                <img src="{{url(\app\User::findOrFail($e->user_id)->avatar)}}"
                                     alt="Contact Person">
                                {{ \app\User::findOrFail($e->user_id)->name }}
                            </div>
                            <br>
                            </p>
                        </div>
                        <div class="row">

                            <div class="col s10 offset-m1 m10 ">
                                <p><b> Description :</b>
                                <pre>{{ $e->description }}</pre>
                                </p>

                            </div>
                            @if($e->user_id == \Illuminate\Support\Facades\Auth::id())
                                <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                                   href="{{ action('EvenementController@showEvent', $e) }}"><i
                                            class="material-icons right">info_outline</i>Details</a>
                                <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                                   href="{{ action('EvenementController@edit', $e) }}"><i
                                            class="material-icons right">code</i>Modifier</a>
                                <a class="btn btn-danger col s10 offset-s1 m3 offset-m1"
                                   href="{{ action('EvenementController@destroy', $e) }}" data-method="delete"
                                   data-confirm="Etes vous certain ?">Supprimer</a>
                            @else
                                <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                                   href="{{ action('EvenementController@showEvent', $e) }}"><i
                                            class="material-icons right">info_outline</i>Details</a>

                            @endif
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="col s12 m4">
            @include('users.sidebar', compact($user=\Illuminate\Support\Facades\Auth::user()))
        </div>
    </div>




@endsection
