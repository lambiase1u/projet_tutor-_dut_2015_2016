<div class="container">
    <div class="row">
        <div class="col s12 m10 offset-m1">
            <div class="card white darken-1">
                <div class="card-content black-text">
                    <span class="card-title black-text top-left ">Evènement</span>
                    <br><br>

                    {!! Form::model($evenement, ['class' => 'form-horizontal', 'url' => action("EvenementController@$action", $evenement), 'method' => $action == "store" ? "Post" : "Put"]) !!}

                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Nom de l'évènement</label>

                            <div class="col s10 offset-s1 m8 ">
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Date de début</label>

                            <div class="col s10 offset-s1 m8 ">
                                {!! Form::input('date', 'date_debut')!!}
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Date de fin</label>

                            <div class="col s10 offset-s1 m8 ">
                                {!! Form::input('date', 'date_fin') !!}
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Ville</label>

                            <div class="col s10 offset-s1 m8 ">
                                {!! Form::text('ville', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Rue</label>

                            <div class="col s10 offset-s1 m8 ">
                                {!! Form::text('rue', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Description</label>
                            <div class="input-field col s10 offset-s1 m8 ">
                                {!!Form::textarea('description',null,['class' =>'materialize-textarea'])!!}
                                <label for="textarea1"><i class=" material-icons small">mode_edit</i></label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col s10 offset-s1 m4 offset-m4">
                                <button type="submit" class="btn btn-primary">
                                    Sauvegarder
                                </button>

                            </div>
                        </div>
                    </div>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
