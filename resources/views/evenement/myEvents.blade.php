@extends('app')
@if($user->id == \Illuminate\Support\Facades\Auth::id())

@section('content')

    <h1>Gérer mes événements</h1>
    @else
        <h1>Evénements de {{$user->name}}</h1>
    @endif

    @if(\Illuminate\Support\Facades\Auth::check())
        <div class="row">
            <div class="col s12 m3">
                <a class="btn btn-primary" href="{{ action('EvenementController@create') }}">Ajouter un évènement</a>
            </div>
        </div>
    @endif

    @if(sizeof($evenement)==0)
        <h3>Vous ne possédez pas d'événement</h3>
    @endif




    <div class="row">
        @foreach($evenement as $e)
            <div class="grid-example col s12 m12">

                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">reorder</i>{{ $e->name }}</div>
                        <div class="collapsible-body">
                            <div class="container">
                                <div class="row">
                                    <table class="table table-striped col s10  offset-s1">
                                        <thead>
                                        <tr>
                                            <th>Date de début</th>
                                            <th>Date de fin</th>
                                            <th>createur</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <td>{{ $e->date_debut }} </td>
                                        <td>{{ $e->date_fin }}</td>
                                        <td>{{ \App\User::findOrFail($e->user_id)->name }}</td>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s10 offset-s2"><pre><p>{{ $e->description }}.</p></pre></div>
                            </div>
                            <div class="row">

                                <a class="waves-effect waves-light btn col s10 offset-s1 m2 offset-m2"
                                   href="{{ action('EvenementController@showEvent', $e) }}"><i
                                            class="material-icons right">info_outline</i>Details</a>

                                @if($e->user_id == \Illuminate\Support\Facades\Auth::id())

                                    <a class="waves-effect waves-light btn col s10 offset-s1 m2 offset-m1"
                                       href="{{ action('EvenementController@edit', $e) }}"><i
                                                class="material-icons right">code</i>Modifier</a>
                                    <a class="btn btn-danger col s10 offset-s1 m2 offset-m1"
                                       href="{{ action('EvenementController@destroy', $e) }}" data-method="delete"
                                       data-confirm="Etes vous certain ?">Supprimer</a>
                                @endif

                            </div>

                        </div>
                    </li>
                </ul>
            </div>
        @endforeach
    </div>

    <br/>
    <div>
        <div class="row">
            <div class="col s10 offset-s1 m6 offset-m3">
                <p class="text-right">
                    <a class="btn btn-primary" href="{{ action('EvenementController@index') }}">Retour aux événements</a>
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <a class="btn btn-primary" href="{{ route('mesEvenements') }}">Retour à mes événements</a>
                    @endif
                </p>
            </div>
        </div>
    </div>
@endsection