@extends('app')


@section('content')


    <h3>Evénement {{ $evenement->name }} </h3>

    @if(\Illuminate\Support\Facades\Auth::check())

    @endif

    <div class="row">
        <div class="col s10 offset-s1 m5">
            <div class="card">
                <div class="col s12 m6">
                    <div class="card-image ">
                        @if($evenement->img == false)
                            <img class="responsive-img" src="{{url().'\imgHold\no-image.png'}}"/>
                        @else
                            <img class="responsive-img" src="{{url().'\img\evenements\\'.$evenement->id.'.jpg'}}"/>
                        @endif
                        @if($evenement->user_id == \Illuminate\Support\Facades\Auth::id())
                            {!! Form::open(['url' => 'photo', 'class' => 'form-horizontal', 'files' => true]) !!}
                            {!! Form::file('img', ['class' => 'form-control'], 'required')!!}
                            {!! Form::hidden('id', $evenement->id) !!}
                            {!! Form::submit('Envoyer !') !!}
                            {!! Form::close() !!}
                        @endif
                    </div>
                </div>
                <div class="col s12 m6">
                    <p>
                        <b>Nom d'évenement :</b> {{ $evenement->name }} <br>
                        <b>date début :</b> {{$evenement->date_debut}} <br>
                        <b>date fin :</b> {{$evenement->date_fin}} <br>
                        <b>Rue : </b> {{$evenement->rue}}<br>
                        <b>Ville :</b> {{$evenement->ville }}<br><br>
                        <b>créateur :</b>
                    <div class="chip">
                        <img src="{{url(\App\User::findOrFail($evenement->user_id)->avatar)}}" alt="Contact Person">
                        {{ \App\User::findOrFail($evenement->user_id)->name }}
                    </div>
                    <br>
                    </p>
                </div>
                <div class="row">

                    <div class="col s10 offset-s1 m10 offset-m1">
                        <p>
                        <pre>{{ $evenement->description }}</pre>
                        </p>

                    </div>
                    @if($evenement->user_id == \Illuminate\Support\Facades\Auth::id())
                        <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                           href="{{ action('EvenementController@showEvent', $evenement) }}"><i
                                    class="material-icons right">info_outline</i>Details</a>
                        <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                           href="{{ action('EvenementController@edit', $evenement) }}"><i
                                    class="material-icons right">code</i>Modifier</a>
                        <a class="btn btn-danger col s10 offset-s1 m3 offset-m1"
                           href="{{ action('EvenementController@destroy', $evenement->id_sport) }}" data-method="delete"
                           data-confirm="Etes vous certain ?">Supprimer</a>
                    @else
                        <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                           href="{{ action('EvenementController@showEvent', $evenement) }}"><i
                                    class="material-icons right">info_outline</i>Details</a>

                    @endif
                </div>
            </div>
        </div>





        @if(sizeof($epreuve)!=0)

            <div class="row">
                <div class="row">
                    <h5 class="col s10 offset-s1  m6 offset-m1">Epreuve pour l'événement {{ $evenement->name }}
                        :</h5>
                    @if(($evenement->user_id == \Illuminate\Support\Facades\Auth::id()))
                        <a class="btn btn-success col m2 offset-m1"
                           href="{{ action('EpreuveController@show', $evenement) }}">Ajouter Epreuve</a>
                    @endif

                <div class="grid-example col s12 m6">

                    <ul class="collapsible popout" data-collapsible="accordion">
                        @foreach( $epreuve as $e)
                            <li>
                                <div class="collapsible-header"><i
                                            class="material-icons">reorder</i>{{  $e->name.' '.$evenement->date_debut}}
                                </div>
                                <div class="collapsible-body">

                                    <div class="row">

                                        @if($evenement->user_id == \Illuminate\Support\Facades\Auth::id())

                                            @if(\App\Http\Controllers\EpreuveController::checkParticiper($e))
                                                <td>
                                                    <a class="waves-effect waves-light btn col s10 offset-s1 m4"
                                                       href="{{ action('EpreuveController@nePlusParticiper', $e) }}">Ne
                                                        plus participer</a></td>
                                            @else
                                                <a class="waves-effect waves-light btn col s10 offset-s1 m3 "
                                                   href="{{ action('EpreuveController@participer', $e) }}">Participer</a>
                                            @endif

                                            <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                                               href="{{ action('EpreuveController@edit', $e) }}"><i
                                                        class="material-icons right">code</i>Modifier</a>
                                            <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                                               href="{{ action('EpreuveController@destroy', $e) }}"
                                               data-method="delete"
                                               data-confirm="Etes vous certain ?">Supprimer</a>
                                        @else
                                            @if(\App\Http\Controllers\EpreuveController::checkParticiper($e))
                                                <a class="waves-effect waves-light btn col s10 offset-s1 m4 offset-m1"
                                                   href="{{ action('EpreuveController@nePlusParticiper', $e) }}">Ne
                                                    plus participer</a>
                                            @else
                                                <a class="waves-effect waves-light btn col s10 offset-s1 m4 offset-m8"
                                                   href="{{ action('EpreuveController@participer', $e) }}">Participer</a>
                                            @endif
                                        @endif

                                    </div>
                                    <div class="row">
                                        <p class="col s10 offset-s2 m4">Durée de l'epreuve : {{ $e->duree }}</p>
                                        @if($e->id_sport == 0)
                                            <div class="col s10 offset-s2 m4">Sport : Inconnu</div>
                                        @else
                                            <div class="col s10 offset-s2 m4">Sport
                                                : {{ \App\Sport::findOrFail($e->id_sport)->nom }}</div>
                                        @endif
                                        <div class="col s12 m10 offset-m1">
                                            <p>
                                            <pre>Description :{{ $e->description }}.</pre>
                                            Lien de partage :<a href="{{url().'/participate/'.$e->token}}">{{url().'/participate/'.$e->token}}</a>
                                            </p>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="row">
                                            <div class=" col m3"> Résultats :</div>
                                            <a class="waves-effect waves-light btn col s10 offset-s1 m3 "
                                               href="{{ action('EpreuveController@showResultat', $e) }}">Consulter</a>
                                            @if(($evenement->user_id == \Illuminate\Support\Facades\Auth::id()))
                                                <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1 "
                                                   href="{{ action('EpreuveController@editResultat', $e) }}">Importer</a>
                                            @endif
                                        </div>
                                        <a class="waves-effect waves-light btn col s10 offset-s1 m5"
                                           href="{{ action('EpreuveController@showParticipants', $e)}}">Liste
                                            participants</a>

                                    </div>

                                </div>
                            </li>
                        @endforeach

                    </ul>
                </div>

            </div>
            <div>

                @else
                    <div class="row">
                        <h5 class="col s10 offset-s1  m6 offset-m1">Epreuve pour l'événement {{ $evenement->name }}
                            :</h5>
                        @if(($evenement->user_id == \Illuminate\Support\Facades\Auth::id()))
                            <a class="btn btn-success col m2 offset-m1"
                               href="{{ action('EpreuveController@show', $evenement) }}">Ajouter Epreuve</a>
                        @endif

                    </div>
                @endif
                <div class="row">
                    <div class="col s10 offset-s1 m6 offset-m3">
                        <p class="text-right">
                            <a class="btn btn-primary" href="{{ action('EvenementController@index') }}">Retour aux
                                événements</a>
                            @if(\Illuminate\Support\Facades\Auth::check())
                                <a class="btn btn-primary" href="{{ route('mesEvenements') }}">Retour à mes
                                    événements</a>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
    </div>
@endsection
