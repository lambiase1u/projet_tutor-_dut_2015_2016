@extends('app')


@section('content')
    <div class="row">
        <div class="col s10 offset-s1  m12">
            <h3>
                <center>Ajouter un nouvel évènement</center>
            </h3>
        </div>
    </div>

    @include('evenement.form', ['action' => 'store'])

@endsection