@extends('app')


@section('content')

    <div class="row">
        <div class="col s10 offset-s1 m6">

            <h2>Gérer les évènements</h2>
        </div>
    </div>
    <div class="row">
        <div class="col s10 offset-s1 m3">
            @if(Auth::check())
                <a class="btn btn-primary" href="{{ action('EvenementController@create')}}">Ajouter un évènement</a>
            @endif
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col s10 offset-s1 m3 ">
            {!! Form::open(array('action' => 'EvenementController@search')) !!}
            {!! Form::text('search', null, ['class' => 'input-field','id'=>'search','type' =>'search' ,
            'placeholder' => 'Saisir une info evenement', 'required']) !!}
            {!! Form::submit('rechercher')!!}
            {!! Form::close() !!}
        </div>
    </div>

    <div class="row">


        @foreach($evenement as $e)


            <div class="grid-example col s12 m6">

                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">reorder</i>{{ $e->name }}</div>
                        <div class="collapsible-body">
                            <div class="container">
                                <div class="row">
                                    <table class="table table-striped col s10  offset-s1">
                                        <thead>
                                        <tr>
                                            <th>Lieu</th>
                                            <th>Date de début</th>
                                            <th>Date de fin</th>
                                            <th>createur</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> {{ $e->ville }}</td>
                                            <td>{{ $e->date_debut }} </td>
                                            <td>{{ $e->date_fin }}</td>
                                            <td>{{ \App\User::findOrFail($e->user_id)->name }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s10">
                                    <pre><p>{{ $e->description }}.</p></pre>
                                </div>
                            </div>
                            <div class="row">


                                @if($e->user_id == \Illuminate\Support\Facades\Auth::id())
                                    <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                                       href="{{ action('EvenementController@showEvent', $e) }}"><i
                                                class="material-icons right">info_outline</i>Details</a>
                                    <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                                       href="{{ action('EvenementController@edit', $e) }}"><i
                                                class="material-icons right">code</i>Modifier</a>
                                    <a class="btn btn-danger col s10 offset-s1 m3 offset-m1"
                                       href="{{ action('EvenementController@destroy', $e) }}" data-method="delete"
                                       data-confirm="Etes vous certain ?">Supprimer</a>
                                @else
                                    <a class="waves-effect waves-light btn col s10 offset-s1 m3 offset-m1"
                                       href="{{ action('EvenementController@showEvent', $e) }}"><i
                                                class="material-icons right">info_outline</i>Details</a>

                                @endif

                            </div>

                        </div>
                    </li>
                </ul>
            </div>
        @endforeach
    </div>
    </div>




@endsection