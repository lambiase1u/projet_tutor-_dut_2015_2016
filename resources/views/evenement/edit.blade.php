@extends('app')


@section('content')

    <h3><center>Modifier l'évènement {{$evenement->name}} </center></h3>

    @include('evenement.form', ['action' => 'update'])

@endsection