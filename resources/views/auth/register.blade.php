@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col s12 m10 offset-m1">
                <div class="card white darken-1">
                    <div class="card-content black-text">
                        <span class="card-title black-text top-left  ">Inscription</span>
                        <br><br>
                        {!! Form::open(['class' => 'form-horizontal']) !!}
                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m2 offset-m1 control-label">Nom d'utilisateur</label>

                                <div class="col s12  m7  ">
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m2 offset-m1 control-label">E-Mail</label>

                                <div class="col s12  m7 ">
                                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m2 offset-m1 control-label">Mot de passe</label>

                                <div class="col s12  m7 ">
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m2 offset-m1 control-label">Confirmer mot de passe</label>

                                <div class="col s12  m7 ">
                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="card-action">
                        <div class="row">
                            <div class="form-group">
                                <center>
                                    <button type="submit" class="btn btn-primary">
                                        S'inscrire
                                    </button>
                                </center>
                            </div>
                        </div>
                    </div>


                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
