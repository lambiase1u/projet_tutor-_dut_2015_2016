@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <br>
            <div class="col s12 m10 offset-m1">
                <div class="card white darken-1">
                    <div class="card-content black-text">
                        <span class="card-title black-text top-left  ">Login</span>
                        <br><br>
                        {!! Form::open(['class' => 'form-horizontal']) !!}
                        <div class="row">
                            <div class="form-group">
                                <label class="col s10 offset-s1 m3 control-label">Nom utilisateur ou email</label>

                                <div class="col s10 offset-s1 m7 ">
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m2 offset-m1 control-label">Mot de passe</label>

                                <div class="col s12  m7 ">
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="card-action">
                        <div class="row">
                            <div class="form-group">
                                <div class="col s10 offset-s2  m4 offset-m2 ">
                                    <button type="submit" class="btn btn-primary">
                                        Se connecter
                                    </button>
                                </div>
                                <div class="col s10 offset-s2  m5 ">
                                    <a class="btn-flat disabled" href="{{ url('/password/email') }}">Mot de passe oublié
                                        ?</a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
