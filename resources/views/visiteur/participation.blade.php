@extends('app')

@section('content')

    <div class="container">
        <div class="row">
            <br>
            <div class="col s12 m10 offset-m1">
                <div class="card white darken-1">
                    <div class="card-content black-text">
                        <span class="card-title black-text top-left  ">Participation</span>
                        <br><br>
                        {!! Form::open(['class' => 'form-horizontal', 'url' => action("VisiteurController@enregistrer")  ]) !!}
                        {!! Form::hidden('epreuve', $epreuve->id) !!}
                        <div class="row">
                            <div class="form-group">
                                <label class="col s10 offset-s1 m1 offset-m2 control-label">Pseudo</label>

                                <div class="col s10 offset-s1 m7 ">
                                    {!! Form::text('pseudo', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m1 offset-m2 control-label">E-Mail</label>

                                <div class="col s12  m7 ">
                                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m1 offset-m2 control-label">Nom</label>

                                <div class="col s12  m7 ">
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m1 offset-m2 control-label">Prénom</label>

                                <div class="col s12  m7 ">
                                    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label class="col s12 m1 offset-m2 control-label">Age</label>

                                <div class="col s12  m7 ">
                                    {!! Form::input('number', 'age', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="card-action">
                        <div class="row">
                            <div class="form-group">
                                <center>
                                    <button type="submit" class="btn btn-primary">
                                        Participer
                                    </button>
                                </center>
                            </div>
                        </div>
                    </div>


                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection
