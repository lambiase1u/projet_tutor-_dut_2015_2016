@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if(sizeof($user)==0 && sizeof($visiteur)==0)
                    <br/>
                    <h3 class="text-center">Il n'y a pas de participants inscris à cette épreuve</h3>
                    <br/>
                @else
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Age</th>

                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($user)!=0)
                            @foreach($user as $u)
                                <tr>
                                    <td>{{ $u->name }}</td>
                                    <td>{{ $u->email }}</td>
                                    <td>{{ $u->firstname }}</td>
                                    <td>{{ $u->lastname }}</td>
                                    <td>{{ $u->age }}</td>
                                </tr>
                            @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                        @endif
                        @if(sizeof($visiteur)!=0)
                            @foreach($visiteur as $v)
                                <tr>
                                    <td>{{ $v->name }}</td>
                                    <td>{{ $v->email }}</td>
                                    <td>{{ $v->firstname }}</td>
                                    <td>{{ $v->lastname }}</td>
                                    <td>{{ $v->age }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
               @endif
            </div>
        </div>
        <div>
            <p class="text-right">
               <br/>
                <a class="btn btn-primary" href="{{ \Illuminate\Support\Facades\URL::previous() }}">Retour</a>
               <a class="btn btn-primary" href="{{ action('EvenementController@index') }}">Retour aux événements</a>
               @if(\Illuminate\Support\Facades\Auth::check())
                   <a class="btn btn-primary" href="{{ route('mesEvenements') }}">Retour à mes événements</a>
               @endif
            </p>
        </div>
    </div>

@endsection