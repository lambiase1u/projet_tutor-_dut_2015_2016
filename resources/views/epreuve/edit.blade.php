@extends('app')


@section('content')

    <h3><center>Modifier l'épreuve {{$epreuve->name}} </center></h3>

    @include('epreuve.form', ['action' => 'update'])

@endsection