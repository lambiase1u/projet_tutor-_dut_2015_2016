@extends('app')


@section('content')
    @if(!empty($epreuve->resultat) && !empty($liste))
        <table>
            <tr>
                <td>nom</td>
                <td>dossard</td>
                <td>temp</td>
            </tr>
            @foreach($liste as $row)
                <tr>
                    <td>{{$row['noms']}}</td>

                    <td>{{$row['dossards']}}</td>

                    <td>{{$row['temps']}}</td>
                </tr>
            @endforeach
        </table>
    @else
        <h3>
            <center>Il n'y a pas de résultats disponibles pour {{$epreuve->name}}</center>
        </h3>
        <br/>
        @if($id_createur == \Illuminate\Support\Facades\Auth::id())
            <div class="panel panel-default">
                <br/>

                <p class="col-md-4 control-label">Vous pouvez mettre en ligne des résultats :</p>
                <a class="btn btn-primary" href="{{ action('EpreuveController@editResultat', $epreuve->id) }}"
                   data-confirm="Etes-vous certain ?">Importer des résultats</a>
                <br/><br/>
            </div>
        @endif
    @endif
    <div>
        <p class="text-right">
            <a class="btn btn-primary" href="{{ action('EvenementController@index') }}">Retour aux événements</a>
            @if(\Illuminate\Support\Facades\Auth::check())
                <a class="btn btn-primary" href="{{ route('mesEvenements') }}">Retour à mes événements</a>
            @endif
        </p>
    </div>

@endsection