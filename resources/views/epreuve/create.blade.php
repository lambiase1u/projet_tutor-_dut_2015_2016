@extends('app')


@section('content')

    <h3><center>Ajouter une nouvelle épreuve à l'evenement n° : {{$epreuve->id_event}} </center></h3>

    @include('epreuve.form',['action' => 'store'])

@endsection