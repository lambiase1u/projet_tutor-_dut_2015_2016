@extends('app')


@section('content')
    <h1>Gérer les épreuves</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <a href="{{ route('epreuve') }}"><th>Nom</th></a>
            <th>Description</th>
            <th>Durée</th>
            <th>Action</th>

        </tr>
        </thead>
        <tbody>
        @foreach( $epreuve as $e)
            <tr>
                <td>{{ $e->id }}</td>
                <td>{{ $e->name }}</td>
                <td><pre><p>{{ $e->description }}</p></pre></td>
                <td>{{ $e->duree }}</td>

                <td>
                    <a class="btn btn-primary" href="{{ action('EpreuveController@edit', $e) }}">Modifier</a>
                    <a class="btn btn-danger" href="{{ action('EpreuveController@destroy', $e) }}" data-method="delete" data-confirm="Etes vous certain ?">Supprimer</a>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection