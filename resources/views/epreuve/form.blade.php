{!! Form::model($epreuve, ['class' => 'form-horizontal', 'url' => action("EpreuveController@$action", $epreuve), 'method' => $action == "store" ? "Post" : "Put"]) !!}

<div class="container">
    <div class="row">
        <div class="col s12 m10 offset-m1">
            <div class="card white darken-1">
                <div class="card-content black-text">
                    <span class="card-title black-text top-left ">Epreuve</span>
                    <br><br>

                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Numero de l'evenement parent</label>

                            <div class="col s10 offset-s1 m8 ">
                                {!! Form::text('id_event', null, ['class' => 'form-control','readonly' => true]) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Nom de l'épreuve</label>

                            <div class="col s10 offset-s1 m8 ">
                                {!! Form::text('name', null, ['class' => 'form-control'], 'required') !!}

                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Sport</label>

                            <div class="col s10 offset-s1 m8 ">
                                <select name="sport" class="browser-default">
                                    <option value="{{$epreuve->id_sport}}" disabled selected>Choisissez un sport</option>
                                    @foreach($sport as $s)
                                        <option value="{{$s->idSport}}">{{$s->nom}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Description</label>

                            <div class="input-field col s10 offset-s1 m8 ">
                                {!!Form::textarea('description',null,['class' =>'materialize-textarea'], 'required')!!}
                                <label for="textarea1"><i class=" material-icons small">mode_edit</i></label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col s10 offset-s1 m2 control-label">Durée de l'épreuve</label>

                            <div class="col s10 offset-s1 m8 ">
                                {!! Form::text('duree', null, ['class' => 'form-control'], 'required') !!}
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="form-group">
                            <div class="col s10 offset-s1 m4 offset-m4">
                                <button type="submit" class="btn btn-primary">
                                    Sauvegarder
                                </button>

                            </div>
                        </div>
                    </div>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


</div>