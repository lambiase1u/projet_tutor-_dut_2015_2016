@extends('app')


@section('content')
    @if($evenement->user_id == \Illuminate\Support\Facades\Auth::id())
        @if(!empty($epreuve->resultat))
            <h3>
                <center>Des résultats ont déjà été importé pour {{$epreuve->name}}</center>
            </h3>
        @else
            <h3>
                <center>Importez vos résultats pour {{$epreuve->name}}</center>
            </h3>
        @endif
        <br/>

        <div class="row">
            <center>
        <p class="text-accent-1">
            Attention ! <br/>
            @if(!empty($epreuve->resultat))
                - Importez des nouveaux résultats effacera les résultats actuels <br/>
            @endif
            - Vous ne pouvez importer que des fichiers avec une extension .xls .xlsx <br/>
        </p>
            </center>
        </div>

        <div class="container">
            <div class="row">
                <div class="col s12 m10 offset-m1">
                    <div class="card white darken-1">
                        <div class="card-content black-text">
                            <span class="card-title black-text top-left ">Epreuve</span>
                            <br><br>

                            {!! Form::open(array('url' => action('EpreuveController@importResultat'),'method'=>'POST','files'=>true)) !!}
                            <div class="row">
                                <div class="form-group">
                                    <div class="col s10 offset-s1 m4">
                                        {!! Form::file('file') !!}
                                        <p class="errors">{!!$errors->first('file')!!}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <a class="btn btn-primary col s10 offset-s1 m2 " href="{{ action('EpreuveController@exportExcel', $epreuve) }}">Excel type</a>
                                {!! Form::hidden('epreuve',$epreuve->id) !!}
                                <div class="form-group">
                                    <div class="col s12  offset-m5 m3">
                                        {!! Form::submit('Importer vos résultats', array('class'=>'btn btn-success'))!!}
                                    </div>
                                </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="row">
                    <p class="text-right">
                        <a class="btn btn-primary col m3 offset-m2" href="{{ action('EvenementController@index') }}">Retour aux
                            événements</a>
                        <a class="btn btn-primary col m4 offset-m1" href="{{ route('mesEvenements') }}">Retour à mes événements</a>
                    </p>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection