(function($){
  $(document).ready(function(){

      $('.collapsible').collapsible({
          accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
      });


   $(".dropdown-button").dropdown();

      $('.slider').slider({full_width: true});
      });



    $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 240
            edge: 'right', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
    );

    $('.materialize-textarea').trigger('autoresize');


})(jQuery); // end of jQuery name space